from .engine import Engine
from .battery import Battery


class Car:
    def __init__(self):
        self.battery = Battery()
        self.engine = Engine()

    def turn_key(self):
        self.engine.start(self.battery.charge)

    def charge_battery(self):
        self.battery.charge = 100

    def __repr__(self):
        return f"Car()"

    def __str__(self):
        msg = f"The car have a battery with {self.battery.charge} % of charge.\n"
        msg += f"The engine is sppings on {self.engine.spin}rotations per minute."
        return msg
