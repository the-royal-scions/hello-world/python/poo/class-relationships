class Battery:
    def __init__(self):
        self.charge = 0

    def __repr__(self):
        return f"Battery()"

    def __str__(self):
        return f"The battery have {self.charge} % charge."
