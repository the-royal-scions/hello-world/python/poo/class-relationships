class Engine:
    def __init__(self):
        self.spin = 0

    def start(self, charge):
        if charge > 50:
            self.spin = 2500

    def __repr__(self):
        return f"Engine()"

    def __str__(self):
        return f"The engine is spinning on {self.spin} rotations per minute."
