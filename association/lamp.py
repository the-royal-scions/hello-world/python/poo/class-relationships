class Lamp:
    def __init__(self, voltage: int):
        self._voltage = None
        self.voltage = voltage
        self.on = False

    @property
    def voltage(self) -> int:
        return self._voltage

    @voltage.setter
    def voltage(self, voltage: int) -> None:
        if not isinstance(voltage, int):
            raise ValueError
        else:
            self._voltage = voltage

    def turn_on(self, voltage: int) -> bool:
        if voltage > self.voltage:
            self.on = True
        return self.on

    def turn_off(self) -> bool:
        self.on = False
        return self.on

    def __repr__(self):
        return f"Lamp(voltage={self.voltage})"

    def __str__(self):
        state = "on" if self.on == True else "off"
        return f"A lamp with {self.voltage}W.\nIt's {state}."
