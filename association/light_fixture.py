from .lamp import Lamp


class LightFixture:
    def __init__(self, energy: int, lamp: Lamp):
        self._energy = None
        self.energy = energy
        self._lamp = None
        self.lamp = lamp

    @property
    def energy(self) -> int:
        return self._energy

    @energy.setter
    def energy(self, energy: int) -> None:
        if not isinstance(energy, int):
            raise ValueError
        else:
            self._energy = energy

    @property
    def lamp(self) -> Lamp:
        return self._lamp

    @lamp.setter
    def lamp(self, lamp: Lamp) -> None:
        if not isinstance(lamp, Lamp):
            raise ValueError
        else:
            self._lamp = lamp

    def turn_lamp_on(self):
        self.lamp.turn_on(self.energy)

    def turn_lamp_off(self):
        self.lamp.turn_off

    def __repr__(self):
        return f"LightFixture(energy={self.energy}, lamp=Lamp(voltage={self.lamp.voltage}))"

    def __str__(self):
        state = "on" if self.lamp.on == True else "off"
        return f"The lamp in the light fixture is {state}"
