from .lamp import Lamp
from .light_fixture import LightFixture

lamp = Lamp(85)
light_fixture = LightFixture(110, lamp)
light_fixture.turn_lamp_on()
light_fixture.turn_lamp_off()
