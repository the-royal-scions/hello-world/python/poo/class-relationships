from .apple import Apple


class AppleBasket:
    def __init__(self):
        self._basket = []

    def add_apple(self, apple: Apple):
        self._basket.append(apple)

    def count_apples(self) -> int:
        return len(self._basket)

    def get_apples(self):
        return self._basket

    def __repr__(self):
        return f"AppleBasket()"

    def __str__(self):
        return f"A basket with {self.count_apples()} apples."
