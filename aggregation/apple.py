class Apple:
    def __init__(self, color: str, size: str):
        self._color = None
        self.color = color
        self._size = None
        self.size = size

    @property
    def color(self) -> str:
        return self._color

    @color.setter
    def color(self, color: str) -> None:
        if not color in ["red", "green", "yellow"]:
            raise ValueError
        else:
            self._color = color

    @property
    def size(self) -> str:
        return self._size

    @size.setter
    def size(self, size: str) -> None:
        if not size in ["small", "medium", "large"]:
            raise ValueError
        else:
            self._size = size

    def __repr__(self):
        return f"Apple(color={self.color}, size={self.size})"

    def __str__(self):
        return f"A {self.size} {self.color} apple."
