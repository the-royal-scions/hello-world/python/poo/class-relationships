from .apple import Apple
from .apple_basket import AppleBasket

red_apple = Apple(color="red", size="small")
green_apple = Apple(color="green", size="large")

apple_basket = AppleBasket()

apple_basket.add_apple(red_apple)
apple_basket.add_apple(green_apple)
