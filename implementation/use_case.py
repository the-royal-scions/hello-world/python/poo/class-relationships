from .warrior import Warrior
from .mage import Mage

mage = Mage()
mage.speak()

warrior = Warrior()
warrior.speak()
