import abc


class Character(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def speak(self) -> str:
        raise NotImplementedError
