class Person:
    def __init__(self, name: str, age: int):
        self._name = None
        self.name = name
        self._age = None
        self.age = age

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, name: str) -> None:
        if not isinstance(name, str):
            raise ValueError
        else:
            self._name = name

    @property
    def age(self) -> int:
        return self._age

    @age.setter
    def age(self, age: int) -> None:
        if not isinstance(age, int):
            raise ValueError
        else:
            self._age = age

    def __repr__(self):
        return f"Person(name=\"{self.name}\", age={self.age})"

    def __str__(self):
        return f"name: {self.name}\nage: {self.age}"
