from .person import Person


class Employee(Person):
    def __init__(self, name: str, age: int, register_number: str):
        super().__init__(name=name, age=age)
        self._register_number = None
        self.register_number = register_number

    @property
    def register_number(self) -> str:
        return self._register_number

    @register_number.setter
    def register_number(self, register_number: str):
        if not self.valid_register_number(register_number):
            raise ValueError
        else:
            self._register_number = register_number

    def valid_register_number(self, register_number: str) -> bool:
        return True

    def __repr__(self):
        return f"Employee(name=\"{self.name}\", age={self.age}, register_number=\"{self.register_number}\")"

    def __str__(self):
        return f"name: {self.name}\nage: {self.age}\n:register number: {self.register_number}"
