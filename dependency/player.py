from .die import Die


class Player:

    def __init__(self, name: str):
        self._name = None
        self.name = name

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, name: str):
        if not isinstance(name, str):
            raise ValueError
        else:
            self._name = name

    def row_die(self, die: Die):
        return die.row()
