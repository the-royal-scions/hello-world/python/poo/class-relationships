from .die import Die
from .player import Player

die = Die(faces=12)

player = Player("Cesar")
player.row_die(die)
