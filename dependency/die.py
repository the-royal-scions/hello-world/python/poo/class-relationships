import random


class Die:
    def __init__(self, faces: int):
        self._faces = None
        self.faces = faces

    @property
    def faces(self) -> int:
        return self._faces

    @faces.setter
    def faces(self, faces: int):
        if not isinstance(faces, int):
            raise ValueError
        else:
            self._faces = faces

    def row(self):
        return random.randint(1, self._faces)
